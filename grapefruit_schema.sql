DROP DATABASE IF EXISTS grapefruittest;
CREATE DATABASE grapefruittest;
USE grapefruittest;


CREATE TABLE round_table (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT
);


CREATE TABLE service (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    service_name    VARCHAR(64)     NOT NULL,
    ip              VARCHAR(16)     NOT NULL,
    port_num        INT             NOT NULL,
    protocol        VARCHAR(8)
);


CREATE TABLE regex (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    pattern         VARCHAR(512)    NOT NULL,
    description     VARCHAR(256)    NOT NULL
);


CREATE TABLE service_regex (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    service         INT             NOT NULL,
    regex           INT             NOT NULL,
    round_start     INT             NOT NULL,
    round_end       INT             NOT NULL,
    active          BOOL            DEFAULT FALSE,
    FOREIGN KEY (service)       REFERENCES      service(id),
    FOREIGN KEY (regex)         REFERENCES      regex(id)
);


CREATE TABLE raw_pcap (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    file_path       VARCHAR(256)    NOT NULL,
    service         INT             NOT NULL,
    round_num       INT             NOT NULL,
    FOREIGN KEY (service)       REFERENCES      service(id),
    FOREIGN KEY (round_num)     REFERENCES      round_table(id)
);


CREATE TABLE conversations (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    file_path       VARCHAR(256)    NOT NULL,
    file_size       INT             NOT NULL,
    num_packets     INT             NOT NULL,
    time_start      TIMESTAMP       NOT NULL,
    time_end        TIMESTAMP       NOT NULL,
    src_ip          VARCHAR(256)    NOT NULL,
    dst_ip          VARCHAR(256)    NOT NULL,
    src_port        INT             NOT NULL,
    dst_port        INT             NOT NULL,
    round_num       INT             NOT NULL,
    service         INT             NOT NULL,
    ignore_flag     BOOL            DEFAULT FALSE,
    maybe_incomp    BOOL            DEFAULT FALSE,
    FOREIGN KEY (round_num)     REFERENCES      round_table(id),
    FOREIGN KEY (service)       REFERENCES      service(id)
);


CREATE TABLE suspiciousness (
    id              INT             NOT NULL    PRIMARY KEY     AUTO_INCREMENT,
    conversation    INT             NOT NULL,
    regex           INT,
    service         INT             NOT NULL,
    round_num       INT             NOT NULL,
    FOREIGN KEY (conversation)  REFERENCES      conversations(id),
    FOREIGN KEY (regex)         REFERENCES      regex(id),
    FOREIGN KEY (service)       REFERENCES      service(id),
    FOREIGN KEY (round_num)     REFERENCES      round_table(id)
);

insert into service values (NULL, "babymarvel", "x.x.x.x", 2001, "TCP");
insert into service values (NULL, "fantasticiot", "x.x.x.x", 2002, "TCP");
insert into service values (NULL, "marvelous", "x.x.x.x", 2003, "TCP");
insert into service values (NULL, "venom", "x.x.x.x", 2004, "TCP");
insert into service values (NULL, "spiderman", "x.x.x.x", 2005, "TCP");
insert into service values (NULL, "hero_text_adventure", "x.x.x.x", 2006, "TCP");
insert into service values (NULL, "shelter", "x.x.x.x", 2007, "TCP");
insert into service values (NULL, "brave_rust", "x.x.x.x", 2008, "TCP");

